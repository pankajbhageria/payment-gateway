var passport = require('passport');
const LocalStrategy = require('passport-local').Strategy
var models = require('./models');
var generate_checksum = require('./lib/generate_checksum');

passport.use(new LocalStrategy({
    usernameField: 'mobile_no',
    passwordField: 'password'
    },
  function(mobile_no, password, done) {
    console.log("local stratergy called");
    findUser(mobile_no, function (err, user) {
      if (err) {
        return done(err)
      }
      if (!user) {
        return done(null, false)
      }

      var password_hash = generate_checksum(password,user.password_salt)

      console.log("entered password hash", password_hash);

      if (password_hash !== user.password_hash  ) {
        return done(null, false)
      }
      return done(null, user)
    })
  }
))



function findUser(mobile_no,callback){
  console.log("findUser called");

  models.User.findOne({
    where: {
      mobile_no: mobile_no
    }
  }).then(function(user){
    if(user){
      callback(null,user)  
    }
    else{
      callback("User not found",null);  
    }
    
  })

	// if(username === user.mobile_no ){
	// 	callback(null,user)
	// }
	// else{
	// 	callback("User not found",null);
	// }

}

function findUserById(id,callback){

  models.User.findById(id).then(function(user){
    if(user){
      callback(null,user)  
    }
    else{
      callback("User not found",null);  
    }
    
  })

	// if(id === user.id ){
	// 	callback(null,user)
	// }
	// else{
	// 	callback("User not found",null);
	// }
}




passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  findUserById(id, function(err, user) {
    done(err, user);
  });
});
