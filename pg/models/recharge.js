var UserModel = require('./user');
var PaymentModel = require('./payment')
// var Payment
// var Merchant
var models = require('./index');

module.exports = function(sequelize, DataTypes) {
  var Recharge = sequelize.define("Recharge", {
	user_id:DataTypes.STRING,
  	amount:DataTypes.INTEGER,
  	instrument_type:DataTypes.STRING,
  	instrument_detail:DataTypes.STRING,
  	status:DataTypes.STRING,
  	paymentId:DataTypes.STRING
  },{
  	instanceMethods:{
		complete:async function(){

			 var self = this;

			  var User = UserModel(sequelize,DataTypes)
			  var Payment = PaymentModel(sequelize,DataTypes)

			  var user = await User.findById(self.user_id);
			  await sequelize.transaction(async function(t){
				  user.balance = user.balance + self.amount;
				  await user.save({transaction:t});
				  self.status = "completed"
				  return await self.save({transaction:t})
			  })
			  console.log("rc6");	

			  if(self.paymentId){
			    var payment = await Payment.findById(self.paymentId)
			    await payment.complete()
			    return payment
			  }

		}  		
  	}
  });
  return Recharge;
};