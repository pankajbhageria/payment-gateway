"use strict";

module.exports = function(sequelize, DataTypes) {
  var Merchant = sequelize.define("Merchant", {
    name:DataTypes.STRING,
    company_name:DataTypes.STRING,
    username: DataTypes.STRING,
    password_hash: DataTypes.STRING,
    password_salt: DataTypes.STRING,
    mobile_no:DataTypes.STRING,
    balance:DataTypes.INTEGER, //in paisa
    pan:DataTypes.STRING,
    address:DataTypes.STRING,
    city:DataTypes.STRING,
    state:DataTypes.STRING,
    pin:DataTypes.STRING,
    country:DataTypes.STRING,
    secret_key:DataTypes.STRING
  });

  return Merchant;
};