//var models = require('./index');
var userModel = require('./user');
var merchantModel = require('./merchant')

module.exports = function(sequelize, DataTypes) {
  var Payment = sequelize.define("Payment", {
	user_id:DataTypes.STRING,
  	amount:DataTypes.INTEGER,
  	merchant_id:DataTypes.STRING,
  	order_id:DataTypes.STRING,
  	recharge_id:DataTypes.STRING,
  	status:DataTypes.STRING,
  	returnFailURL:DataTypes.STRING,
  	returnSuccessURL:DataTypes.STRING
  },{

    instanceMethods:{
      //reduce user balance
      //add merchant balance
      complete:async function(){
        var self = this;
        var User = userModel(sequelize,DataTypes);
        var Merchant = merchantModel(sequelize,DataTypes);

        var user = await User.findById(self.user_id)
        console.log("pg1",user)
        var merchant = await Merchant.findById(self.merchant_id)
        await sequelize.transaction(async function (t) {
          console.log("pg2",user)
          user.balance = user.balance - self.amount;
          console.log("pg3")
          await user.save({transaction:t});
          console.log('pg4')
          merchant.balance = merchant.balance + self.amount;
          console.log("pg5")
          await merchant.save({transaction:t});
          console.log('pg6')
          self.status = "complete";
          return await self.save({transaction:t})
        })        
      }
    }

  });
  return Payment;
};