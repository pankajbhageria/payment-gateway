"use strict";

module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define("User", {
    name:DataTypes.STRING,
    mobile_no:DataTypes.STRING,
    password_hash: DataTypes.STRING,
    password_salt: DataTypes.STRING,
    balance:DataTypes.INTEGER, //in paisa
    pan:DataTypes.STRING,
    address:DataTypes.STRING,
    city:DataTypes.STRING,
    state:DataTypes.STRING,
    pin:DataTypes.STRING,
    country:DataTypes.STRING
  });

  return User;
};