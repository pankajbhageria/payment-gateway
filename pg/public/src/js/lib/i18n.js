var objectPath = require('object-path');

module.exports = function(dictionary){
	return function(key,locale){
		return objectPath(dictionary,locale,key)
	}
}