var React = require('react');
var ReactDOM = require('react-dom');
var Login = require('./components/login');
var Payment = require('./components/payment');
var page = require('page');
var request = require('superagent');


page('/login',function(ctx,next){

	var queryParams = getQueryParams();


	//if user doesnot exist, try loading user, still if user doesnot exist load else go to payment page

	var onLogin = function(user){
		window.user = user;
		page('/payment');
	}
	ReactDOM.render(<Login onLogin={onLogin} mobile_no={queryParams.mobile_no}/>,document.getElementById("container"));
	//next();

});

function getUser(callback){
	request.get('/current_user').set('Accept', 'application/json').end(function(err,res){
		callback(null,res.body.user);
	})
}

function getQueryParams(qs) {
	if(!qs)
	{	
		qs = document.location.search
	}	
    qs = qs.split('+').join(' ');

    var params = {},
        tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
    }

    return params;
}

page('/payment',function(ctx,next) {
	var user = window.user;
	//if user does not exist then redirect to login page
	var queryParams = getQueryParams();
	console.log("queryParams",queryParams)

	if(!user){
		getUser(function(err,user){
			window.user = user;//TODO: store user in a store

			if(!user){
				page('/login');
				return;
			}
			ReactDOM.render(<Payment payment_id={queryParams.payment_id} walletBalance={user.balance} paymentAmount={queryParams.amount}/>,document.getElementById('container'))
		}); 
	}
	else{
		ReactDOM.render(<Payment payment_id={queryParams.payment_id} walletBalance={user.balance} paymentAmount={queryParams.amount} />,document.getElementById('container'))
	}
})
page({hashbang:true});


page('/payment');










