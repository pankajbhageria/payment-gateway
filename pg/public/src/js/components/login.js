var React = require('react');
var ReactDOM = require('react-dom');
//var API = require('../apis');
//var lscache = require('lscache');
//var page = require('page');
var request = require('superagent');


var Login = React.createClass({

	getInitialState:function(){

		return {error:false}
	},

	handleSubmit :function(e){

		var self = this;


		e.preventDefault();

		var mobile_no = ReactDOM.findDOMNode(this.refs.mobile_no).value;
		var password = ReactDOM.findDOMNode(this.refs.password).value;



		var params = {mobile_no:mobile_no,password:password};

		console.log(params);
		var req = request.post('/login').send(params);

		req.end(function(err,res){
			if(err){
				self.setState({"error":err.error});//handle error in logging in
				return;
			}
			//set user
			//window.current_user = res.user;
			self.props.onLogin(user);
		})

// 		API.session.login({params:params},function(err,response){
// 			console.log(response);
// //			if(response.success)
// //			{
// 			if(err){
// 				self.setState({"error":err.error});//handle error in logging in
// 				return;
// 			}
// 			lscache.set('sessionID',response.sessionID);
// 			lscache.set('loggedIn',true);
// 			console.log(lscache.get('sessionID'));
// 			page('/payment');

// //			}
// //			else
// //			{
// //			
// //			}
// 		})

	},

	cancel:function(){
		page('/');
	},

	render:function(){
		var message;
		if(this.state.error)
		{
			message = <p className="red">{this.state.error}</p>
		}
		return (
			<div className="login">
				<img onClick={this.cancel} style={{position:"absolute", right:"20px",top:"20px"}} className="close-button" src="/images/cross.png" width="32"/>
			    <div className="container text-center">
			        <br/><br/>
			        <center><a href="/"><img className="logo block" width="250" src="images/logo-megaexams.png"/></a></center>
			        <br/><br/>
			        <h3 className="lighter">Welcome back!</h3><br/>
			        	{message}
			            <div className="col-sm-offset-4 col-sm-4 text-center">
			            <form action="/subjects" method="get">
			              <div className="form-group">
			                <input required="" ref="mobile_no" type="text" name="mobile_no" placeholder="Mobile Number" className="input-lg" defaultValue={this.props.mobile_no}/>
			              </div>
			              <div className="form-group">
			                <input required="" ref="password" type="password" name="password" placeholder="Password" className="input-lg"/>
			              </div>
			              <input type="hidden" name="redirect" value=""/>
			              <input type="submit" onClick={this.handleSubmit} name="login" className="blue-transparent button-large" value="&nbsp; LOG IN &nbsp;"/>
			            </form>
			        </div>
			    </div>
			</div>



		)

	}

});

module.exports = Login;