var React = require('react');
var ReactDOM = require('react-dom');
var Card = require('react-credit-card');


var DebitCard = React.createClass({

	getInitialState:function(){

		return {
				number:{value:"",valid:null},
				focused:"number",
				cvv:{value:"",valid:null},
				name:{value:"",valid:null},
				expiryMonth:{value:"",valid:null},
				expiryYear:{value:"",valid:null}
			}

	},


	validateName:function(value){

		if(value == "" || !value){
			return false
		}
		else{
			return true;
		}

	},

	validateCvv:function(value){
		var reg = /^\d+$/;
		if(value.length < 3 || value.length > 4){
			return false;
		}
		else if(reg.test(value)){
			return true
		}
		else{
			return false;
		}

	},

	validateCard:function(value){

		// accept only digits, dashes or spaces
		if(/[^0-9-\s]+/.test(value)) return false;

		// The Luhn Algorithm. It's so pretty.
		var nCheck = 0, nDigit = 0, bEven = false;
		value = value.replace(/\D/g, "");

		for(var n = value.length - 1; n >= 0; n--){
			var cDigit = value.charAt(n),
				  nDigit = parseInt(cDigit, 10);

			if (bEven) {
				if ((nDigit *= 2) > 9) nDigit -= 9;
			}

			nCheck += nDigit;
			bEven = !bEven;
		}
		return (nCheck % 10) == 0;



	},


	updateField:function(fieldName){
		var value = ReactDOM.findDOMNode(this.refs[fieldName]).value;
		var validations = {number:this.validateCard,cvv:this.validateCvv,name:this.validateName}
		var focused = this.state.focused;
		if(fieldName == "cvv"){
			focused = "cvc"
		}
		else{
			focused = "number"
		}	

		var validation = validations[fieldName];



		var obj = this.state[fieldName];
		if(validation){
			obj.valid = validation(value);
		}
		obj.value = value
		this.setState({[fieldName]:obj,focused:focused})

	},

	onCvvFocus:function(){
		this.setState({focused:"cvc"})
	},

	onCvvBlur:function(){
		this.setState({focused:"number"})
	},


	render:function(){

		var expiryDate = this.state.expiryMonth.value + "/" + this.state.expiryYear.value;

//				<Card number={this.state.number.value} name={this.state.name.value} cvc={this.state.cvv.value} focused={this.state.focused} expiry={expiryDate}/>


		return (
			<div className="row">




				<div className="col-xs-2"></div>
				<div className="col-xs-8">
					<h4>Use Debit Card</h4>
					<div>
						<span className="small">(Rs {this.props.paymentAmount/100} will be deducted )</span>
					</div>
					<hr/>
					<div className="debit-card">
						<h3>Debit Card</h3>
						<div>
					        <label className="ccontrol-label" for="card-number">Enter your card number</label>
					        <div className="">
					          <input type="text" className={"form-control " + (this.state.number.valid === false ? "error":"") } ref="number" onChange={this.updateField.bind(this,"number")} name="card-number" id="card-number" placeholder="Debit/Credit Card Number"/>
					        </div>
					    </div>
					    <div className="row date-cvv-row">
					    <div className="col-xs-6">
					    	<label>Valid upto</label>
					    	<div>
					    		<input className="form-control" name="expiry-month" id="expiry-month" placeholder="MM" size="2" />
					    		<input className="form-control" name="expiry-year" id="expiry-year" placeholder="YY" size="2" />
					    	</div>
					    </div>

					    <div className="col-xs-6">
					    	<label id="cvv-label">CVV code</label>
					    	<div className="clearfix"></div>
					    	<div>
					    		<input className="form-control" name="cvv" ref="cvv" onFocus={this.onCvvFocus} onBlur={this.onCvvBlur} onChange={this.updateField.bind(this,"cvv")} id="cvv" placeholder="" size="2" />
					    	</div>
					    </div>

					    </div>


					</div>
				</div>
				<div className="col-xs-2"></div>
			</div>

		)

	}

});

module.exports = DebitCard;



/**
				    <fieldset>
				      <input name="instrument_type" value="Debit" type="hidden"/> 
				      <legend>Use Debit Card</legend>(Rs {this.props.paymentAmount/100} will be deducted )

				      <div className="form-group">
				        <label className="col-sm-3 control-label" for="card-number">Card Name</label>
				        <div className="col-sm-9">
				          <input type="text" className={"form-control " + (this.state.name.valid === false? "error":"") } ref="name" onChange={this.updateField.bind(this,"name")} name="card-name" id="card-name" placeholder="Name on the Card"/>
				        </div>
				      </div>


				      <div className="form-group">
				        <label className="col-sm-3 control-label" for="card-number">Card Number</label>
				        <div className="col-sm-9">
				          <input type="text" className={"form-control " + (this.state.number.valid === false ? "error":"") } ref="number" onChange={this.updateField.bind(this,"number")} name="card-number" id="card-number" placeholder="Debit/Credit Card Number"/>
				        </div>
				      </div>
				      <div className="form-group">
				        <label className="col-sm-3 control-label" for="expiry-month">Expiration Date</label>
				        <div className="col-sm-9">
				          <div className="row">
				            <div className="col-xs-3">
				              <select className="form-control col-sm-2" ref="expiryMonth" onChange={this.updateField.bind(this,"expiryMonth")} name="expiry-month" id="expiry-month">
				                <option>Month</option>
				                <option value="01">Jan (01)</option>
				                <option value="02">Feb (02)</option>
				                <option value="03">Mar (03)</option>
				                <option value="04">Apr (04)</option>
				                <option value="05">May (05)</option>
				                <option value="06">June (06)</option>
				                <option value="07">July (07)</option>
				                <option value="08">Aug (08)</option>
				                <option value="09">Sep (09)</option>
				                <option value="10">Oct (10)</option>
				                <option value="11">Nov (11)</option>
				                <option value="12">Dec (12)</option>
				              </select>
				            </div>
				            <div className="col-xs-3">
				              <select className="form-control" name="expiry-year" ref="expiryYear" onChange={this.updateField.bind(this,"expiryYear")}>
				                <option value="16">2016</option>
				                <option value="17">2017</option>
				                <option value="18">2018</option>
				                <option value="19">2019</option>
				                <option value="20">2020</option>
				                <option value="21">2021</option>
				                <option value="22">2022</option>
				                <option value="23">2023</option>
				                <option value="24">2024</option>
				                <option value="25">2025</option>
				                <option value="26">2026</option>
				                <option value="27">2027</option>
				                <option value="28">2028</option>
				                <option value="29">2029</option>
				                <option value="30">2030</option>
				                <option value="31">2031</option>
				                <option value="32">2032</option>
				                <option value="33">2033</option>

				              </select>
				            </div>
				          </div>
				        </div>
				      </div>
				      <div className="form-group">
				        <label className="col-sm-3 control-label" for="cvv">Card CVV</label>
				        <div className="col-sm-3">
				          <input type="text" className={"form-control " + (this.state.cvv.valid === false? "error":"") } name="cvv" ref="cvv" onFocus={this.onCvvFocus} onBlur={this.onCvvBlur} onChange={this.updateField.bind(this,"cvv")} id="cvv" placeholder="Security Code"/>
				        </div>
				      </div>
				    </fieldset>

**/