var React = require('react');
var ReactDOM = require('react-dom');
var DebitCard = require('./debitCard');
var request = require('superagent');
//import CardReactFormContainer from 'card-react';



var Payment = React.createClass({


	getInitialState:function(){

		return {csrf:null,number:null}

	},



	componentDidMount:function(){
		var self = this;
		request.post("/get_csrf").set('Accept', 'application/json').end(function(err,res){

			self.setState({csrf:res.body.csrf})
		})
	},




	render:function(){

		var walletBalance = this.props.walletBalance;
		var paymentAmount = this.props.paymentAmount;
		var payment_id = this.props.payment_id;

		var pendingAmount = paymentAmount - walletBalance;

		var pendingPaymentBlock = false;

		if(pendingAmount > 0){

			pendingPaymentBlock = <div>
									<div className="row">
										<div className="col-xs-2"></div>
										<div className="col-xs-8 pending-row">
											<div className="col-xs-2"></div>
											<div className="col-xs-8 title">To complete the payment you need to add : </div>
											<div className="col-xs-2 amount">Rs {pendingAmount/100}</div>
										</div>
										<div className="col-xs-2"></div>

								  	</div>
								  	<hr/>
								  	<div>
										<DebitCard paymentAmount={paymentAmount}/>
								  	</div>
								</div>
										
		}

		return (
				<div style={{paddingTop:"100px"}}>
					<div className="row">
						<div className="col-xs-2"></div>
						<div className="col-xs-8 total-row" >
							<div className="col-xs-2 icon">
								<img style={{width:"60px"}} src="https://rukminim1.flixcart.com/www/prod/images/flipkart_logo_retina-9fddfff2.png" />
							</div>
							<div className="col-xs-8 title">
							 	Total Payment to be made to Flipkart: 
							 </div>
							 <div className="col-xs-2 amount" style={{fontSize:"15px",fontWeight:"bold"}}>
							 	Rs {paymentAmount/100}
							 </div>
						</div>
					</div>
					<div className="row">
						<div className="col-xs-2">
							
						</div>
						<div  className="col-xs-8 wallet-row">
							<div className="col-xs-2 icon">
								<img src="/images/green-tick.png" width="30px" style={{textAlign:"right"}}/>
							</div>
							<div className="col-xs-8 title">
									Use Wallet Balance: 

							</div>
							<div className="col-xs-2 amount">
								Rs {walletBalance/100}
							</div>
						</div>
						<div className="col-xs-2" style={{fontSize:"15px",fontWeight:"bold"}}>
						</div>
					</div>

					<form action="/payment_complete" method="post">
						<input type="hidden" name="_csrf" value={this.state.csrf}/>
						<input type="hidden" name="payment_id" value={payment_id}/>
						<input type="hidden" value={walletBalance} name="walletBalance"/>
						{pendingPaymentBlock}
						<div className="form-group">
						<div className="col-sm-offset-2 col-sm-9">
						  <button type="submit" className="btn btn-success" id="pay-now">Pay Now</button>
						</div>
						</div>
				    </form>



					<div id="card-wrapper"></div>				    


				</div>
		)

	}

});

module.exports = Payment;

