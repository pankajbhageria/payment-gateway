// file:app/authenticate/init.js
const passport = require('passport')  
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session      = require('express-session');
var path    = require("path");
var express = require('express');
var logger = require('morgan');
//var csrf = require('csurf')
//var csrfProtection = csrf({ cookie: true })
var models = require('./models');
var app = express();
//var checksum_generator = require('./lib/checksum_generator');

app.use(logger('dev'));
require('./auth.js');

app.use(cookieParser('asdfa sfdasfaffas fsdf as')); // read cookies (needed for auth)
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({ extended: false }));
app.use(session({ secret: 'asdfa sfdasfaffas fsdf as' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions




// app.use(function(req,res,next){
//   console.log("session",req.session)
//   console.log("cookies",req.cookies)
//   next();
// })








app.use(express.static(path.join(__dirname, 'public/dist')));


require('./controllers')(app);







// const user = {  
//   mobile_no: '8971521507',
//   password: 'pankaj',
//   id: 1,
//   balance:100
// }





module.exports = app;
