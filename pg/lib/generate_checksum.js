var crypto = require('crypto');

module.exports = function generate_checksum(data,secret){
    var input = data+secret;
    return crypto
        .createHash('md5')
        .update(input, 'utf8')
        .digest('hex')
}