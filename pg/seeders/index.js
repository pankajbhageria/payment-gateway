const sequelize_fixtures = require('sequelize-fixtures');

//a map of [model name] : model
//see offical example on how to load models
//https://github.com/sequelize/express-example/blob/master/models/index.js
const models = require('../models'); 

//from file
console.log(__dirname);
//can use glob syntax to select multiple files
sequelize_fixtures.loadFile('seeders/*.json', models).then(function(){
    console.log("seeding done");
});