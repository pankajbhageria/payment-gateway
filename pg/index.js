require("babel-register");
require("babel-polyfill");

var models = require('./models');
var app = require('./app.js');

models.sequelize.sync().then(function() {
  app.listen(process.env.PORT || 3000);
})