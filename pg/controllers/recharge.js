module.exports = function(app){
var models = require('../models');

//paymentRecord
//{id,order_id,m_id,amount,walletAmount,instrumentType,instrumentId,status:created/pending/completed}
//return point for debit card or any other instrument
//input paymentId, status: success/fail, reason:""
app.get('/recharge_completed',async function(req,res){

  var rechargeId = req.query.rechargeId || req.body.rechargeId;

  console.log('rechargeId',rechargeId);
  var recharge = await models.Recharge.findById(rechargeId);

  try{
    var payment = await recharge.complete()
    res.redirect(payment.returnSuccessURL); //TODO: pass other data like checksum, orderId etc
  }
  catch(e){
    console.log("charge failed",e);
  }

})


}