var csrf = require('csurf')
var csrfProtection = csrf({ cookie: true })
var models = require('../models');
var generate_checksum = require('../lib/generate_checksum')
module.exports = function(app){
var path    = require("path");

app.post('/payment_complete',csrfProtection,async function(req,res){
  //deduct from wallet

  var paymentId = req.body.payment_id;

  var payment = await models.Payment.findById(paymentId);

  if(!payment.user_id){
    payment.user_id = req.user.id;
    await payment.save();
  }

  //payment should exist and should be pending

  var merchant = await models.Merchant.findById(payment.merchant_id);

  var user = req.user;
  var amount = payment.amount;
  var walletBalance = user.balance;
  var pendingAmount = payment.amount - user.balance;

  if(payment.amount > user.balance)
  {
    //if debit card redirect to debit card
      //charge debit card
      //authorize debit card otp
      var recharge = await models.Recharge.create(
      { user_id:req.user.id,
          amount:pendingAmount,
          instrument_type:"debit",
          instrument_details:JSON.stringify(req.body.instrument_details),
          paymentId:paymentId,
          status:"pending"
      })
      res.redirect("http://localhost:7000/charge?rechargeId="+recharge.id)
  }
  else{
    //deduct balance

    await payment.complete()
    res.redirect(payment.returnSuccessURL);
  }  
  
})


app.post('/payment',async function(req,res){

  var body = req.body;

  //find the merchant
  //check the checksum
  //create payment record
  //

  //check checksum
  var body 
  var data = {amount:body.amount,
              merchant_id:body.merchant_id,
              mobile_no:body.mobile_no,
              order_id:body.order_id,
              status:"pending",
              returnFailURL:body.return_fail_url,
              returnSuccessURL:body.return_success_url
              }

  
  var merchant = await models.Merchant.findById(data.merchant_id);



  if(!merchant){
    res.redirect(data.returnFailURL+"?reason=Merchant not found");
    return;
  }
  var dataStr = JSON.stringify(data);

  var checksum = generate_checksum(dataStr,merchant.secret_key);

  if(checksum !== body.checksum){
  	console.log("checksum didnot match",checksum)
    res.redirect(data.returnFailURL+"?reason=Checksum did not match");
    return
  }

  var current_user = req.user;
  if(current_user)
  {
    data.user_id = current_user.id;
  }

  var payment = await models.Payment.create(data);




	//redirect to payment page with processId, phone, email
	res.redirect('/payment?order_id='+data.order_id+"&merchant_id="+ data.merchant_id+"&amount="+data.amount+"&mobile_no="+data.mobile_no+"&payment_id="+payment.id);

	})



app.get('/payment',csrfProtection,function(req,res){
	     req.session.csrf = req.csrfToken();
	   	 res.sendFile(path.join(__dirname+'/../index.html'));
	})



}