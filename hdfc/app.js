// file:app/authenticate/init.js
const passport = require('passport')  
const LocalStrategy = require('passport-local').Strategy
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session      = require('express-session');
var path    = require("path");
var express = require('express');
var logger = require('morgan');

var app = express();
app.use(logger('dev'));

app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser()); // get information from html forms


app.use(session({ secret: 'asdfa sfdasfaffas fsdf as' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions




app.get('/charge',function(req,res){
  //charge the card and then authorize
  //logic for charge
	console.log("rechargeId",req.query.rechargeId);
  res.redirect('/authorize?rechargeId='+req.query.rechargeId);
})

app.get('/authorize',function(req,res){

	  res.sendFile(path.join(__dirname+'/index.html'));
})

app.post('/authorize',function(req,res){
    //check the password and authorize
    res.redirect('http://localhost:3000/recharge_completed?rechargeId='+req.body.rechargeId)

})


// app.get('/login')

// app.post('/login',
//   passport.authenticate('local', { successRedirect: '/',
//                                    failureRedirect: '/login',
//                                    failureFlash: true })
// );

app.listen(process.env.PORT || 7000);





