// file:app/authenticate/init.js
var path    = require("path");
var express = require('express');
var logger = require('morgan');
var app = express();
app.use(logger('dev'));


app.get('/',function(req,res){

	  res.sendFile(path.join(__dirname+'/index.html'));
})

app.get('/payment_completed',function(req,res){
    res.sendFile(path.join(__dirname+'/payment_completed.html'));
})

app.get('/payment_failed',function(req,res){
    res.sendFile(path.join(__dirname+'/payment_failed.html'));
})


var port = process.env.PORT || 5000;
app.listen(port);
console.log("Listening on port: ",port);
